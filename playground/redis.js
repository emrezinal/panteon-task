const redisClient = require('redis').createClient;

const redisPort = 6379;
const redisHost = '127.0.0.1';

const redis = redisClient(redisPort, redisHost);

redis.on('error', (e) => {
  console.log(e);
});

redis.on('connect', () => {
  console.log('Redis client connected');
});

redis.set('key1', 'value', redis.print);
redis.get('key1', (err, reply) => {
  console.log('value is: ' + reply);
});
module.exports = { redis };
