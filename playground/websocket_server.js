const express = require('express');
const { createServer } = require('http');
const WebSocket = require('ws');

const app = express();

const server = createServer(app);
const wss = new WebSocket.Server({ host: '192.168.43.53', port: 8081, server });
// const wss_logout = new WebSocket.Server({ host: '192.168.42.8', port: 8081, server, path: '/logout' });
// const wss_leaderboard = new WebSocket.Server({ host: '192.168.42.8', port: 8081, server, path: '/leaderboard' });


// connect to ws://10.70.19.120:8081
wss.on('connection', function (ws) {
  const id = setInterval( function () {
    ws.send(JSON.stringify(process.memoryUsage()), function() {
      //
      // Ignore errors.
      //
    });
  }, 100);
  console.log('started client interval');

  ws.on('close', function() {
    console.log('stopping client interval');
    clearInterval(id);
  });
});

server.listen(8080, function () {
  console.log('Listening on http://localhost:8080');
});
