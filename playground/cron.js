const cron = require("node-cron");

// "* * 21 * *" means 21st of every month
// every second
cron.schedule("* * * * * *", function() {
  console.log("running a task every second");
});

// every day
cron.schedule("0 0 0 * * *", function() {
  console.log("her gun basi");
});

// every week
cron.schedule("* * * * * 1", function() {
  console.log("running a task every monday");
});