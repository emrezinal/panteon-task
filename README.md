# Panteon-task
## Usage

After you clone it, cd into project directory and use:

```bash
npm start #This command runs the server on local
```

If you want to use cloud server,it is always online at ip: ---

I wrote two different clients in the client/ folder.
There are already users signed up in mongodb. I hardcoded their ids and websocket tickets into these client js files.
So you can just use them but when you quit these processes, server will automatically remove their websocket tickets so you have to request another ticket and hardcode it in the client file. I will show you how at below.

First you have to sign up to rest api. You get a token in headers when you do that. Using that token and your user id you can request a ticket in /user/:id/requestTicket . You have to enter token in headers as key: x-auth value : <token> . Then enter that ticket and id into the websocket clients and you can start them with:
 ```bash
node ws_online1.js #This command send predefined amount of money to server
node ws_leaderboard.js #This command gets the leaderboard one time
```
Beware that when you close these your ticket gets destroyed so you have to get another ticket and hardcode them into clients.

More detail in user.route and user.model files. 

## Cloud

I uploaded project to DigitalOcean server(Ubuntu 18.04 1GB/25GB disk) and please don't make too much (exceeding hundreds of GB)
requests because i have the most basic server right now. (5$/month) I started the project with pm2 module because i don't want an unhandled error to stop process.

## What could be done better

- I could have refactored more but because of my tight deadline i only focussed on making the system work.   
- Right now i can't use Redis cache because i got an error when i tried to get first hundred users from cache. This is because of asynchronous structure of Nodejs. So i get all data from mongodb when a user tries to get leaderboard.
- I could have use Promise.all in more places, and this would speed up the process.
- I could have check mongodb logs for slow queries. (It logs queries passing time limits of a threshold.)
- I could have make more db operations async.
- I could have add some projections into db operations. This would determine what information would return from these operations.

## Tech stack
- Nodejs
- Express
- Mongodb (also using mongoose)
- Json web tokens (authentication)
- Websockets (npm ws module)
- Cron (node-cron module)
- Pm2 (process manager working at server)
- Redis (didn't work but can be done)
- DigitalOcean server (Ubuntu 18.04 1GB/25GB disk)
