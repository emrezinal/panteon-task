const WebSocket = require('ws');

const host = '165.22.29.47';
const port = 8081;
const id1 = '5d9afa543a4b657418ee1444';
const ticket1 = 'b9b3aaec-658c-4844-b277-c09cb7f94600';
const pathname1 = 'online';
const moneyChange = 4;
var connection1 = new WebSocket(`ws://${host}:${port}/${pathname1}/id=${id1}?${ticket1}`);
connection1.onopen = function () {
  // connection is opened and ready to use
  //ws.send('this is a message from the client...');
  setInterval(() => {
    connection1.send(moneyChange);    
  }, 1000);
};

connection1.onerror = function (error) {
  // an error occurred when sending/receiving data
  console.log(error);
};

connection1.onmessage = function (message) {
  console.log('[/online] first users money: ' + JSON.parse(message.data));
};
