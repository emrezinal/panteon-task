const redisClient = require('redis').createClient;

const redisPort = process.env.REDIS_PORT;
const redisHost = process.env.REDIS_HOST;

const redis = redisClient(redisPort, redisHost);

redis.on('error', (e) => {
  console.log(e);
});

redis.on('connect', () => {
  console.log('Redis client connected');
});

module.exports = { redis };
