const express = require('express');
const bodyParser = require('body-parser');
require('./db/mongoose');
const { server } = require('./websocket/websockets');

const ws_port = process.env.WS_PORT;
const ws_host = process.env.WS_HOST;
const ex_port = process.env.EXPRESS_PORT;
const ex_host = process.env.EXPRESS_HOST;

const userRouter = require('./routes/user.route');

const app = express();

app.use(bodyParser.json());
app.use(userRouter);

// Start daily and weekly tasks
require('./core/leaderboard').dailyTask.start();
require('./core/leaderboard').weeklyTask.start();

// For websockets
server.listen(ws_port, ws_host, 1, function() {
  console.log(`Websockets are listening on ${ws_host}:${ws_port}`);
});

// For Express Rest Api
app.listen(ex_port, ex_host, () => {
  console.log(`Rest api is listening on ${ex_host}:${ex_port}`);
});

module.exports = { app };
