const http = require('http');
const WebSocket = require('ws');
const url = require('url');
const { redis } = require('./../redis/redis');
const { User } = require('./../models/user.model');
const { app } = require('./../index');
const { getUserAround, getFirstHundred } = require('./../core/leaderboard');

const server = http.createServer(app);
// User will communicate through this path via websocket connection when s/he wants to access leaderboard
const wss_leaderboard = new WebSocket.Server({ noServer: true });
// User will communicate through this path via websocket connection while s/he is online
const wss_online = new WebSocket.Server({ noServer: true });

// Following upgrade event is documented at:
// https://nodejs.org/api/http.html#http_event_upgrade
server.on('upgrade', (request, socket, head) => {
  console.log('on server upgrade...');
  // parse the request ticket from search part of the url ws://192.168.42.8:8081/id=<id_here>?<ticket_here>
  const pathname = (url.parse(request.url).pathname).split('/id')[0];
  const ticket = (url.parse(request.url).search).split('?')[1];
  const id = (url.parse(request.url).pathname).split('=')[1];
  // check if ticket exists in redis or db
  User.checkTicket(id, ticket).then((res) => {
    console.log(`[onServerUpgrade] res: ${res}`);
    checkTicketResult(pathname, res, request, socket, head);
  });
});

wss_online.on('connection', (ws, request) => {
  console.log('[online] started client interval');
  const ticket = (url.parse(request.url).search).split('?')[1];
  const id = (url.parse(request.url).pathname).split('=')[1];
  // system expects money change amount (increase or decrease) every second while user is online
  // system expects client only sends an integer a second which is money change
  ws.on('message', (moneyChange) => {
    console.log(`[Websocket online channel] money change is: ${moneyChange}`);
    const everySecond = () => {
      User.findOneAndUpdate({ _id: id }, { $inc: { account: moneyChange } }).then((user) => {
        if (!user) {
          console.log('[Websocket online channel] There is no such user with the given id!');
        }
        ws.send(user.account);
      }).catch((e) => {
        console.log(e);
      });
    };
    everySecond();
  });

  ws.on('close', () => {
    close_ws(ticket, id);
  })
});

wss_leaderboard.on('connection', async (ws, request) => {
  console.log('[leaderboard] started client interval');
  const ticket = (url.parse(request.url).search).split('?')[1];
  const id = (url.parse(request.url).pathname).split('=')[1];
  let userAround;
  await getUserAround(id).then((arr) => {
    userAround = arr;
  });

  // redis.exists('firstHundred', async (err, reply) => {
  //   if (!err) {
  //     if (reply === 1) {
  //       let userList;
  //       console.log('firstHundred redis key exists');
  //       await redis.lrange('firstHundred', 0, -1, (err1, list) => {
  //         if (err1) {
  //           console.log('[redis lrange function] error1' + err1);
  //         } else {
  //           userList = list;
  //           console.log(`list is: ${userList}`);
  //         }
  //       });
  //       console.log(`list: ${userList}\nuser: ${userAround.cu}\nuserAround: ${userAround.aru}`);
  //       // ws.send({ 'list': userList, 'user': userAround.cu, 'userAround': userAround.aru });

  //     } else {
  //       console.log('firstHundred redis key doesn`t exist');
  //       getUserAround(id).then((obj) => {
  //         // console.log(obj)
  //         getFirstHundred().then((arr) => {
  //           obj1 = { obj, arr };
  //           ws.send(JSON.stringify(obj1, null, 2));
  //         })
  //         console.log(`[wss_leaderboard] array coming from getUserAround ${JSON.stringify(obj, null, 2)}`);
  //       });
  //     }
  //   } else {
  //     console.log('[redis exists function] error ' + err);
  //   }
  // });
  getUserAround(id).then((obj) => {
    // console.log(obj)
    getFirstHundred().then((arr) => {
      obj1 = { obj, arr };
      ws.send(JSON.stringify(obj1, null, 2));
    })
    console.log(`[wss_leaderboard] array coming from getUserAround ${JSON.stringify(obj, null, 2)}`);
  });
  ws.on('close', () => {
    close_ws(ticket, id);
  });
});

const close_ws = (ticket, id) => {
  console.log('Bravo 6 going dark (client is going offline), and destroying ticket');

  console.log('id: ', id, 'ticket: ', ticket);
  User.findByIdAndUpdate(id, { $set: { ticket: null } }, { new: true, runValidators: true }).then((user) => {
    if (!user) {
      console.log('No such user');
    }
    if (!user.ticket) { console.log('Ticket destroyed!'); } else { console.log('Ticket is still there!!'); }
  }).catch((e) => {
    console.log(e);
  });
};

// is websocket connection ticket authorized
const checkTicketResult = async (pathname, res, request, socket, head) => {
  const ifFalse = async (socket) => {
    console.log('socket destroyed');
    await socket.destroy();
  };

  const ifTrue = async (pathname, request, socket, head) => {
    console.log('Ticket authorized.');
    if (pathname === '/online') {
      await wss_online.handleUpgrade(request, socket, head, function (ws) {
        wss_online.emit('connection', ws, request);
      });
    } else if (pathname === '/leaderboard') {
      await wss_leaderboard.handleUpgrade(request, socket, head, function (ws) {
        wss_leaderboard.emit('connection', ws, request);
      });
    }
  };

  if (res === false) {
    await ifFalse(socket);
  } else {
    await ifTrue(pathname, request, socket, head);
  }
};

module.exports = { server };
