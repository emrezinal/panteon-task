const cron = require('node-cron');
const { redis } = require('./../redis/redis');
const { User } = require('./../models/user.model');

const task = async () => {
  // sort by account in descending order
  await User.find({}).sort({ account: -1 }).then(async (users) => {
    let userCount = users.length;
    for (let i = 0; i <= users.length - 1; i++) { // starting from the user with highest account (first one)
      let newRank = i + 1;
      let currentUserRank = users[i].rank;
      let rankChange = currentUserRank - newRank;
      console.log(`${i}. users id: ${users[i]._id} current Rank: ${currentUserRank} rankchange: ${rankChange}`);
      await User.updateOne({ _id: users[i]._id }, { $set: { rank: newRank, rankChange } }).then((updatedUser) => {
        console.log(`[dailyTask] Updated users rank: ${updatedUser}`);
      });
    }
    if (userCount > 100) {
      userCount = 101;
    } else if (userCount < 1) {
      console.log(`[task] userCount is: ${userCount}`);
      return;
    } else {
      userCount += 1;
    }
    console.log(`redis will store: ${userCount} users`);

    for (let i = 1; i < userCount; i++) {
      await User.findByRank(i).then((user) => {
        console.log(`user is: ${user}`);
        if (user) { console.log(`Did redis pushed for rank ${i}: ${redis.rpush('firstHundred', JSON.stringify(user))}`) }
      });
    }
  });
}

// Update ranks and last rank change on every days start 0 0 0 * * *
// Redis update first hundred on cache
const dailyTask = cron.schedule('0 0 0 * * *', async () => {
  task();
});

// every week (monday) * * * * * 1
const weeklyTask = cron.schedule("0 0 * * MON", async () => {
  // *make a reward pool 2% and reward people accordingly
  await weeklyRewarding();
  // *turn all ranks and rankChanges to 0
  await User.find({}).then(async (users) => {
    for (let i = 0; i <= users.length - 1; i++) { // starting from the user with highest account (first one)
      console.log(`${i} current Rank: ${users[i].rank} current rank Change: ${users[i].rankChange}`);
      await User.updateOne({ _id: users[i]._id }, { $set: { rank: 0, rankChange: 0 } });
    }
  });
});

const getFirstHundred = async () => {
  let firstHundred = [];
  for (let i = 1; i < 101; i++) {
    await User.findByRank(i).then((user) => {
      if (user) {
        firstHundred.push(user);
      }
    });
  }
  return firstHundred;
};

const getUserAround = async (id) => {
  let currentUserRank;
  let currentUser;
  await User.findById(id).then((user) => {
    if (!user) {
      console.log(`[getUserAround] Couldnt find user with id`);
    }
    currentUserRank = user.rank;
    currentUser = user;
  });
  // 3 before 2 after
  let aroundUsers;
  await getAroundUsers(currentUserRank).then((au) => {
    aroundUsers = au;
  }).catch((err) => {
    console.log(err);
  });
  console.log(`current user:`, currentUser, `\naround users: `, aroundUsers);
  return { cu: currentUser, aru: aroundUsers };
};

const getAroundUsers = async (currentUserRank) => {
  let aroundUser = [];
  if (currentUserRank > 100) {
    switch (currentUserRank) {
      case 101:
        await User.findByRank(102).then((user) => { aroundUser.push(user) });
        await User.findByRank(103).then((user) => { aroundUser.push(user) });
        break;
      case 102:
        await User.findByRank(101).then((user) => { aroundUser.push(user) });
        await User.findByRank(103).then((user) => { aroundUser.push(user) });
        await User.findByRank(104).then((user) => { aroundUser.push(user) });
        break;
      case 103:
        await User.findByRank(101).then((user) => { aroundUser.push(user) });
        await User.findByRank(102).then((user) => { aroundUser.push(user) });
        await User.findByRank(104).then((user) => { aroundUser.push(user) });
        await User.findByRank(105).then((user) => { aroundUser.push(user) });
        break;
      default:
        await User.findByRank(currentUserRank - 3).then((user) => { aroundUser.push(user) });
        await User.findByRank(currentUserRank - 2).then((user) => { aroundUser.push(user) });
        await User.findByRank(currentUserRank - 1).then((user) => { aroundUser.push(user) });
        await User.findByRank(currentUserRank + 1).then((user) => { aroundUser.push(user) });
        await User.findByRank(currentUserRank + 2).then((user) => { aroundUser.push(user) });
        break;
    }
  }
  return aroundUser;
}

// Fetch all users money
const weeklyRewarding = async () => {
  await User.find({}).then((users) => {
    const moneyArr = () => {
      let element = [];
      for (let i = 0; i < users.length; i++) {
        element.push(users[i].account);
      }
      return element;
    }
    const calculateTotal = () => {
      let total = 0;
      return new Promise(async (resolve) => {
        const arr = moneyArr();
        console.log('Money array is: ', arr);
        arr.map((elm) => {
          total += elm;
        });
        resolve(total);
      });
    };
    const distributeRewards = async (rank, rewardPool) => {
      let incrAmount;
      await User.findByRank(rank).then(async (user) => {
        if (!user) {
          console.log(`[distributeRewards] Couldnt find user with rank: ${rank}. Make sure there are more than 100 users!`);
          return;
        }
        if (rank === 1) { incrAmount = rewardPool / 5; } else if (rank === 2) { incrAmount = rewardPool * 1.5 / 10; } else if (rank === 3) { incrAmount = rewardPool / 10; } else if (rank >= 4 && rank <= 100) { incrAmount = rewardPool * 55 / 97 };
        console.log(`[distributeRewards] rank is ${rank} and incrementAmount is ${incrAmount}`);

        await User.findByIdAndUpdate(user._id, { $inc: { account: incrAmount } }, { new: true, runValidators: true }).catch((e) => {
          console.log(e);
        });
      });
    };
    calculateTotal().then(async (totalMoney) => {
      console.log('Total money is: ', totalMoney);
      let peopleCount;
      if (users.length < 100) {
        peopleCount = users.length + 1;
      } else {
        peopleCount = 101;
      }
      const rewardPool = totalMoney / 50; // 2% of total money is in rewarding pool
      Promise.all([distributeRewards(1, rewardPool), distributeRewards(2, rewardPool), distributeRewards(3, rewardPool)]).catch((error) => {
        console.log(`Error in promises ${error}`)
      });
      console.log(`People count var is: ${peopleCount}`);
      for (let i = 4; i < peopleCount; i++) { // people count var is 1 more than users.length
        await distributeRewards(i, rewardPool);
      }
    });
  });
}

module.exports = {
  task,
  dailyTask,
  weeklyTask,
  getFirstHundred,
  getUserAround,
  weeklyRewarding,
};
