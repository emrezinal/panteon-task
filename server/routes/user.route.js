const express = require('express');

const UserController = require('./../controllers/user.controller');
const { authenticate } = require('./../middleware/authenticate');

const router = new express.Router();

// User API
router.get('/user/me', authenticate, UserController.getCurrentUser); // get current user by token (my profile page)
router.get('/user/:id', authenticate, UserController.getById); // get user by id (somebody else's or current users profile page)
router.get('/user/:id/requestTicket', authenticate, UserController.requestTicket); // send a ticket for ws connection
// router.get('/users', UserController.all);
router.post('/user', UserController.add); // sign up call also logs in the user (adds a token)
router.post('/user/login', UserController.login); // login call (adds a new token)
router.patch('/user/:id/changePassword', authenticate, UserController.changePassword); // for changePassword screen
router.patch('/user/:id', authenticate, UserController.edit); // edit user info
router.delete('/user/me', authenticate, UserController.logout); // logout route (deletes token)
router.delete('/user/:id', authenticate, UserController.deleteAccount); // are you sure question or password request are needed

module.exports = router;
