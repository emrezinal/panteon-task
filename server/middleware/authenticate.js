const { User } = require('./../models/user.model');

// Basic user authentication by json web tokens
// Used in situations where only a logged in user can access
const authenticate = (req, res, next) => {
  const token = req.header('x-auth');

  User.findByToken(token).then((user) => {
    if (!user) {
      return Promise.reject();
    }
    req.user = user;
    req.token = token;
    next();
  }).catch((e) => {
    res.status(401).json({
      err: 'You are not authorized to make this request!',
    }); // 401 : unauthorized
  });
};

module.exports = {
  authenticate,
};
